package com.hop.noisli;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

public class ListSongAdapter extends BaseAdapter {

    private Context mContext;
    private List<Song> mSongs;

    public ListSongAdapter(Context context, List<Song> Songs) {
        mContext = context;
        mSongs = Songs;
    }

    @Override
    public int getCount() {
        return mSongs.size();
    }

    @Override
    public Song getItem(int position) {
        return mSongs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static class ViewHolder {
        TextView nameSong;
        TextView singer;
        View container;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_song, parent, false);
            holder = new ViewHolder();

            // init XML
            holder.nameSong = (TextView) convertView.findViewById(R.id.nameSong);
            holder.singer = (TextView) convertView.findViewById(R.id.singer);
            holder.container = convertView.findViewById(R.id.container);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setValue(holder, position);

        return convertView;
    }

    private void setValue(ViewHolder holder, int position) {
        Song song = getItem(position);

        holder.nameSong.setText(song.getName());
    }

}
