package com.hop.noisli;


/**
 * Created by Hop on 09/10/2015.
 */
public class Song {
    private String name;
    private String singer;
    private String songId;
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public Song(String name, String singer, String songId) {
        this.name = name;
        this.singer = singer;
        this.songId = songId;
    }

    public Song() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }
}
