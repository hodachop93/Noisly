package com.hop.noisli;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Handler;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgViewRain;
    private MediaPlayer mRainMP;
    ImageView imgViewCoffee;
    private MediaPlayer mCoffeeMP;

    private static final float DEFAULT_VOLUME = 0.4f;

    SeekBar seekBarRain, seekBarCoffee;
    ListView listView;

    private List<Song> songList;
    private ListSongAdapter mAdapter;
    private boolean isLoadList;
    private int lastPosition = -1;
    private String mLinkStream;
    private MediaPlayer mBacgroundMP;

    private static final String BASE_URL_SEARCH = "http://quynhluu3.org/api/search/search?q=";
    private static final String BASE_URL_DETAIL = "http://quynhluu3.org/api/search/detail?q=";

    private String search = "kiss the rain";
    ImageView btnPrevious, btnPlay, btnNext;
    TextView tvSongName;

    TextView tvCurrentTime, tvDurationTime;
    SeekBar seekBarMediaPlayer;

    private Handler mHandler = new Handler();
    int start = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*try {
            createDataSample();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        imgViewRain = (ImageView) findViewById(R.id.imgViewRain);
        imgViewRain.setOnClickListener(this);
        imgViewRain.setSelected(false);
        imgViewRain.getDrawable().setAlpha(125);

        imgViewCoffee = (ImageView) findViewById(R.id.imgViewCoffee);
        imgViewCoffee.setOnClickListener(this);
        imgViewCoffee.setSelected(false);
        imgViewCoffee.getDrawable().setAlpha(125);

        seekBarMediaPlayer = (SeekBar) findViewById(R.id.seekBarMediaPlayer);
        tvCurrentTime = (TextView) findViewById(R.id.tvCurrentTime);
        tvDurationTime = (TextView) findViewById(R.id.tvDurationTime);



        /*mRainMP = MediaPlayer.create(this, R.raw.rain);
        mRainMP.setLooping(true);
        mRainMP.setVolume(DEFAULT_VOLUME, DEFAULT_VOLUME);


        mCoffeeMP = MediaPlayer.create(this, R.raw.coffee);
        mCoffeeMP.setLooping(true);
        mCoffeeMP.setVolume(DEFAULT_VOLUME, DEFAULT_VOLUME);*/

        btnPrevious = (ImageView) findViewById(R.id.btnPrevious);
        btnPlay = (ImageView) findViewById(R.id.btnPlay);
        btnNext = (ImageView) findViewById(R.id.btnNext);
        tvSongName = (TextView) findViewById(R.id.tvSongName);
        btnPlay.setOnClickListener(this);


        seekBarRain = (SeekBar) findViewById(R.id.seekBarRain);
        seekBarCoffee = (SeekBar) findViewById(R.id.seekBarCoffee);

        listView = (ListView) findViewById(R.id.listView);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        //execute load list song
        isLoadList = true;
        new DownloadTask(this).execute(search);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < songList.size(); i++) {
                    songList.get(i).setSelected(false);
                }
                songList.get(position).setSelected(true);
                mAdapter.notifyDataSetChanged();

                if (lastPosition == position) {
                } else {
                    Song currentSong = songList.get(position);
                    String detail = currentSong.getSongId();
                    tvSongName.setText(currentSong.getName());
                    //Execute load a specific song
                    isLoadList = false;

                    new DownloadTask(getApplicationContext()).execute(detail);
                }
                lastPosition = position;
            }
        });

        seekBarRain.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float vol = (float) progress / 100;
                mRainMP.setVolume(vol, vol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarCoffee.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float vol = (float) progress / 100;
                mCoffeeMP.setVolume(vol, vol);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekBarMediaPlayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mBacgroundMP != null & fromUser) {
                    mBacgroundMP.seekTo(progress * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.imgViewRain) {
            clickOnImageViewRain();
        } else if (id == R.id.imgViewCoffee) {
            clickOnImageViewCoffee();
        } else if (id == R.id.btnPlay) {
            if (btnPlay.isSelected()) {
                mBacgroundMP.pause();
                btnPlay.setSelected(false);
            } else {
                mBacgroundMP.start();
                btnPlay.setSelected(true);
            }
        }
    }

    private void clickOnImageViewRain() {
        Drawable drawable = imgViewRain.getDrawable();
        if (imgViewRain.isSelected()) {
            imgViewRain.setSelected(false);
            drawable.setAlpha(125);
            seekBarRain.setVisibility(View.INVISIBLE);
            mRainMP.pause();
        } else {
            imgViewRain.setSelected(true);
            drawable.setAlpha(255);
            mRainMP.start();
            seekBarRain.setVisibility(View.VISIBLE);

        }
    }

    private void clickOnImageViewCoffee() {
        Drawable drawable = imgViewCoffee.getDrawable();
        if (imgViewCoffee.isSelected()) {
            imgViewCoffee.setSelected(false);
            drawable.setAlpha(125);
            seekBarCoffee.setVisibility(View.INVISIBLE);
            mCoffeeMP.pause();
        } else {
            imgViewCoffee.setSelected(true);
            drawable.setAlpha(255);
            mCoffeeMP.start();
            seekBarCoffee.setVisibility(View.VISIBLE);
        }
    }

    class DownloadTask extends AsyncTask<String, Void, String> {
        private Context mContext;

        public DownloadTask(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected String doInBackground(String... params) {
            String search = params[0];
            StringBuilder json = new StringBuilder();
            try {
                String urlStr;
                if (isLoadList) {
                    urlStr = BASE_URL_SEARCH + URLEncoder.encode(search, "utf-8");
                } else {
                    urlStr = BASE_URL_DETAIL + URLEncoder.encode(search, "utf-8");
                }

                URL url = new URL(urlStr);
                URLConnection conn = url.openConnection();

                conn.connect();

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));


                String temp;
                while ((temp = reader.readLine()) != null)
                    json.append(temp);
                reader.close();


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return json.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (isLoadList) {
                    songList = getListSongFromJson(s);
                    mAdapter = new ListSongAdapter(mContext, songList);
                    listView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                } else {
                    getLinkStreamFromJson(s);
                    if (mBacgroundMP != null) {
                        mBacgroundMP.stop();
                        mBacgroundMP.reset();
                    } else {
                        mBacgroundMP = new MediaPlayer();
                        mBacgroundMP.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    }
                    mBacgroundMP.setDataSource(mLinkStream);
                    mBacgroundMP.prepareAsync();
                    mBacgroundMP.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            btnPlay.setSelected(true);
                            int duration = mBacgroundMP.getDuration();
                            tvDurationTime.setText(convertToSecond(duration));
                            seekBarMediaPlayer.setMax(duration / 1000);
                            mBacgroundMP.start();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (mBacgroundMP != null) {
                                        int currentPosition = mBacgroundMP.getCurrentPosition() / 1000;
                                        seekBarMediaPlayer.setProgress(currentPosition);
                                        tvCurrentTime.setText(convertToSecond(mBacgroundMP.getCurrentPosition()));
                                    }
                                    mHandler.postDelayed(this, 1000);

                                }
                            });
                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private String convertToSecond(int duration) {
            Date time = new Date(duration);
            SimpleDateFormat format = new SimpleDateFormat("mm:ss");
            return format.format(time);
        }

        private void getLinkStreamFromJson(String s) throws JSONException {
            JSONObject root = new JSONObject(s);
            JSONObject linkDownload = root.getJSONObject("link_download");
            mLinkStream = linkDownload.getString("128");


        }

        private List<Song> getListSongFromJson(String json) throws JSONException {
            List<Song> list = new ArrayList<>();
            JSONObject root = new JSONObject(json);
            JSONArray key = root.getJSONArray("Key");
            for (int i = 0; i < key.length(); i++) {
                JSONObject obj = key.getJSONObject(i);
                String name = obj.getString("Name");
                String singer = obj.getString("Singer");
                String songId = obj.getString("SongID");
                list.add(new Song(name, singer, songId));
            }
            return list;
        }
    }


}
